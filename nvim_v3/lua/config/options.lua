vim.g.mapleader = ","
vim.g.maplocalleader = ","

vim.opt.autochdir = true -- change dir to current file dir
vim.opt.autoindent = true -- new line indent is same as previous
vim.opt.autoread = true -- automatically read file that has been changed outside vim
vim.opt.backupcopy = "yes" -- disables safe write to enable hot module replacement
vim.opt.breakindent = true -- line wrap keeps indentation
vim.opt.clipboard = "unnamedplus" -- have clipboard be system clipboard
vim.opt.cmdheight = 0 -- height of the command section
vim.opt.colorcolumn = "80" -- put colored column
vim.opt.completeopt = "menuone,noselect" -- options for complete menus
vim.opt.conceallevel = 0 -- amount visual should reflect what it is
vim.opt.cursorline = true -- find current line quickly
vim.opt.encoding = "utf-8" -- displayed encoding
vim.opt.expandtab = true -- change tab to spaces
vim.opt.filetype = "on" -- detect filetypes
vim.opt.foldexpr = "nvim_treesitter#foldexpr()" -- method to use for folding
vim.opt.foldlevel = 0 -- initial fold level
vim.opt.foldmethod = "expr" -- method to use for folding
vim.opt.formatoptions = "jcroqlnt" -- various options for formatting
vim.opt.gdefault = true -- makes all substitute be global
vim.opt.hidden = true -- allow buffer switching with unsave files
vim.opt.hlsearch = false -- disable highlight search
vim.opt.laststatus = 1 -- adds status if at least 2 files
vim.opt.nrformats = "alpha" -- characters are considered for incrementing
vim.opt.number = true -- show line numbers
vim.opt.previewheight = 5 -- Height of the preview window
vim.opt.relativenumber = true -- line number is relative to cursor
vim.opt.scrolloff = 10 -- minimum number of lines to keep below and above cursor
vim.opt.shiftwidth = 2 -- tab width
vim.opt.showcmd = false -- don't show last command as last line of screen
vim.opt.showmatch = true -- show matching bracket
vim.opt.showmode = false -- don't show the mode as last line
vim.opt.signcolumn = "yes" -- if sign column should always be present
vim.opt.softtabstop = 2 -- tab width
vim.opt.splitbelow = true -- vertical split is always below
vim.opt.swapfile = false -- disable swap file
vim.opt.tabstop = 2 -- tab width
vim.opt.termguicolors = true -- use terminal colors
vim.opt.textwidth = 0 -- disable limit of chars to paste
vim.opt.timeoutlen = 300 -- time to wait for mapped sequence to complete
vim.opt.undofile = true -- save undos once file is closed
vim.opt.undolevels = 1000 -- number of undos to save
vim.opt.undoreload = 10000 -- number of lines to save for undo
vim.opt.updatetime = 250 -- update time delay
vim.opt.wildmode = "longest:full,full" -- mode for finding files
vim.opt.wrapmargin = 0 -- prevent line breaks

vim.opt.iskeyword:remove("-,:,#")
vim.opt.listchars:append("space:·")
vim.opt.listchars:append("eol:↴")

vim.opt.wildignore:append("*.pyc")
vim.opt.wildignore:append("**/__pycache__/*")
vim.opt.wildignore:append("*.zip")
vim.opt.wildignore:append("*.tar")

vim.opt.dictionary:append("/usr/share/dict/words")
vim.opt.dictionary:append("/usr/share/dict/french")

-- disable other users of ,
vim.keymap.set({ "n", "v" }, ",", "<nop>", { silent = true })
