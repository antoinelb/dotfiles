local opts = { silent = true, noremap = true }

-- auto indent
vim.keymap.set("n", "i", function()
	if #vim.fn.getline(".") == 0 then
		return [["_cc]]
	else
		return "i"
	end
end, { expr = true })

-- scrolling
vim.keymap.set("n", "j", "gjzz", opts)
vim.keymap.set("n", "k", "gkzz", opts)

-- canadian multilingual keyboard
vim.keymap.set("n", "é", "/", opts)
vim.keymap.set("n", "qé", "q/", opts)

-- formatting
vim.keymap.set("n", "<leader>s", ":%!python -m json.tool --sort-keys<cr>", opts)

-- empty lines
vim.keymap.set("n", "<a-j>", ":set paste<cr>o<esc>k :set nopaste<cr>", opts)
vim.keymap.set("n", "<a-k>", ":set paste<cr>O<esc>j :set nopaste<cr>", opts)

-- comment boxes
vim.keymap.set("v", "<leader>b", "!boxes<cr>", opts)
vim.cmd([[
  autocmd filetype python vnoremap <localleader>b :!boxes -d shell<cr>
]])

-- folding
vim.keymap.set("n", "<space>", "@=(foldlevel('.') ? 'za' : '<space>')<cr>", opts)

-- quickfix
vim.keymap.set("n", "<leader>q", "<cmd>ccl<cr>", opts)
