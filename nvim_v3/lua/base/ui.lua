return {
	{
		"nvim-tree/nvim-web-devicons",
		config = { default = true },
	},
	{
		"shaunsingh/nord.nvim",
		lazy = false,
		config = function()
			vim.g.nord_contrast = true
			vim.g.nord_borders = true
			vim.g.nord_disable_background = true
			vim.g.nord_italic = true
			vim.g.nord_bold = true
			require("nord").set()
		end,
	},
	{
		"stevearc/dressing.nvim",
		event = "VeryLazy",
		opts = {},
	},
	{
		"rcarriga/nvim-notify",
		event = "VeryLazy",
		opts = {
			background_colour = "#000000",
		},
	},
}
