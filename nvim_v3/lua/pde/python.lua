return {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "python", "toml" })
			vim.treesitter.query.set(
				"python",
				"folds",
				[[
        [
          (function_definition)
          (class_definition)
          (string)
        ] @fold
      ]]
			)
		end,
	},
	{
		"jose-elias-alvarez/null-ls.nvim",
		opts = function(_, opts)
			local nls = require("null-ls")

			table.insert(opts.sources, nls.builtins.formatting.isort)
			table.insert(
				opts.sources,
				nls.builtins.formatting.black.with({
					args = { "--stdin-filename", "$FILENAME", "--quiet", "--line-length", "79", "-" },
				})
			)
			table.insert(opts.sources, nls.builtins.diagnostics.mypy)
		end,
	},
	{
		"williamboman/mason.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "debugpy", "black", "ruff", "isort", "mypy" })
		end,
	},
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				ruff_lsp = {},
				pyright = {
					settings = {
						python = {
							analysis = {
								autoImportCompletions = true,
								typeCheckingMode = "off",
								autoSearchPaths = true,
								useLibraryCodeForTypes = true,
								diagnosticMode = "openFilesOnly",
								stubPath = vim.fn.stdpath("data") .. "/lazy/python-type-stubs/stubs",
							},
						},
					},
				},
			},
			setup = {
				ruff_lsp = function()
					local lsp_utils = require("base.lsp.utils")
					lsp_utils.on_attach(function(client, _)
						if client.name == "ruff_lsp" then
							client.server_capabilities.hoverProvider = false
						end
					end)
				end,
			},
		},
	},
	{
		"nvim-neotest/neotest",
		dependencies = {
			"nvim-neotest/neotest-python",
		},
		opts = function(_, opts)
			vim.list_extend(opts.adapters, {
				require("neotest-python")({
					runner = "pytest",
				}),
			})
		end,
	},
	{
		"danymat/neogen",
		opts = function(_, opts)
			opts.languages["python"] = {
				template = {
					annotation_convention = "numpydoc",
				},
			}
		end,
	},
	{
		"microsoft/python-type-stubs",
		cond = false,
	},
	{
		"kalekundert/vim-coiled-snake",
		ft = "python",
	},
}
