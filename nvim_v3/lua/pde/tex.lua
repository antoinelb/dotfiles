local ignored_errors = {
	"Underfull",
	"Overfull",
	"Package balance",
	"Unused global",
	"Package fancyhdr Warning",
	"There's no line here to end",
	"No hyphenation patterns",
	"include should only be used after",
}
return {
	{
		"nvim-treesitter/nvim-treesitter",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "latex" })
			vim.list_extend(opts.highlight.additional_vim_regex_highlighting, { "latex" })
		end,
	},
	{
		"jose-elias-alvarez/null-ls.nvim",
		opts = function(_, opts)
			local nls = require("null-ls")

			table.insert(opts.sources, nls.builtins.formatting.latexindent)
		end,
	},
	{
		"williamboman/mason.nvim",
		opts = function(_, opts)
			vim.list_extend(opts.ensure_installed, { "texlab", "ltex-ls" })
		end,
	},
	{
		"neovim/nvim-lspconfig",
		opts = {
			servers = {
				texlab = {
					settings = {
						texlab = {
							diagnostics = {
								ignoredPatterns = ignored_errors,
							},
						},
					},
				},
				ltex = {
					on_attach = function(_, _)
						require("ltex_extra").setup({
							load_langs = { "en-CA", "fr" },
							init_check = true,
							path = vim.fn.expand("~") .. "/.local/share/ltex",
						})
					end,
					settings = {
						ltex = {
							enabled = { "latex" },
							language = "auto",
						},
					},
				},
			},
			setup = {
				ruff_lsp = function()
					local lsp_utils = require("base.lsp.utils")
					lsp_utils.on_attach(function(client, _)
						if client.name == "ruff_lsp" then
							client.server_capabilities.hoverProvider = false
						end
					end)
				end,
			},
		},
	},
	{
		"lervag/vimtex",
		ft = "tex",
		config = function()
			vim.g.tex_flavor = "latex"
			vim.g.vimtex_compiler_method = "latexmk"
			vim.g.vimtex_fold_enabled = true
			vim.g.vimtex_format_enabled = true
			vim.g.vimtex_view_enabled = true
			vim.g.vimtex_view_automatic = true
			vim.g.vimtex_view_method = "zathura"
			vim.g.vimtex_view_forward_search_on_start = false
			vim.g.vimtex_compiler_latexmk = {
				build_dir = "",
				callback = true,
				continuous = true,
				executable = "latexmk",
				hooks = {},
				options = {
					"-verbose",
					"-file-line-error",
					"-synctex=1",
					"-interaction=nonstopmode",
					"-shell-escape",
				},
			}
			vim.g.vimtex_quickfix_ignore_filters = ignored_errors
		end,
	},
}
