# set -g default-terminal "tmux-256color-italic"
set -g default-terminal "tmux-256color"
set-option -ga terminal-overrides ",xterm-256color:Tc"

# prefix remap
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix

# fix cursor
set -ga terminal-overrides ',*:Ss=\E[%p1%d q:Se=\E[2 q'

# split panes
bind '#' split-window -h -c '#{pane_current_path}'
bind - split-window -v -c '#{pane_current_path}'
unbind '"'
unbind %

# pane switching
bind-key h select-pane -L
bind-key j select-pane -D
bind-key k select-pane -U
bind-key l select-pane -R

# move windows
bind-key S-Left swap-window -t -1
bind-key S-Right swap-window -t +1

# resize-pane to single line
bind-key t resize-pane -y 2

# set working directory
bind-key w attach -c "#{pane_current_path}"

# reload config
bind-key r source-file ~/.tmux.conf

# time before escape key works
set -s escape-time 0

setw -g mode-keys vi
setw -g status-keys vi
bind -T copy-mode-vi h send-keys -X cursor-left
bind -T copy-mode-vi j send-keys -X cursor-down
bind -T copy-mode-vi k send-keys -X cursor-up
bind -T copy-mode-vi l send-keys -X cursor-right

bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "xclip -i -f -selection primary | xclip -i -selection clipboard"

bind p split-window -v -c '#{pane_current_path}' -d "ipython"

set -g history-limit 999999
 
###############
### Plugins ###
###############

# set -g @plugin 'tmux-plugins/tpm'
# set -g @plugin 'tmux-plugins/tmux-sensible'
#
# run '~/.tmux/plugins/tpm/tpm'
# run-shell -b 'pomodoro server'

#######################
### Vim integration ###
#######################

is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key h if-shell "$is_vim" "send-keys C-a h"  "select-pane -L"
bind-key j if-shell "$is_vim" "send-keys C-a j"  "select-pane -D"
bind-key k if-shell "$is_vim" "send-keys C-a k"  "select-pane -U"
bind-key l if-shell "$is_vim" "send-keys C-a l"  "select-pane -R"
bind-key , if-shell "$is_vim" "send-keys C-a ," "select-pane -l"
bind-key -T copy-mode-vi h select-pane -L
bind-key -T copy-mode-vi j select-pane -D
bind-key -T copy-mode-vi k select-pane -U
bind-key -T copy-mode-vi l select-pane -R
bind-key -T copy-mode-vi , select-pane -l

######################
### User interface ###
######################

# start window numbers at 1
set -g base-index 1
set-window-option -g pane-base-index 1

# renumber windows when window closed
set -g renumber-windows on
set -g status-interval 1

# notifications
setw -g monitor-activity on
set -g visual-activity off
 
# panes
# set -g pane-border-fg black
# set -g pane-active-border-fg blue

# status bar design
# status line
set -g status on
set -g status-justify left
set -g status-bg black
set -g status-fg colour141
set -g status-interval 1

# messaging
# set -g message-fg white
# set -g message-bg colour24
# set -g message-command-fg blue
# set -g message-command-bg black
 
# window status
setw -g window-status-separator " "
setw -g status-justify centre
setw -g window-status-format "#I #W"
setw -g window-status-current-format "#[bg=colour55,fg=colour253] #I #[bg=colour55,fg=colour253]#W "
setw -g window-status-style bg=black
 
# status left
set -g status-left ""
 
set-option -g visual-bell off
set-option -g visual-silence off
set-window-option -g monitor-activity off
set-option -g bell-action none

# modes
setw -g clock-mode-colour colour135
# setw -g mode-attr bold
# setw -g mode-fg colour135
# setw -g mode-bg default

# status right
set -g status-right ""
