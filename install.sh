#! /usr/bin/sh

DIR=$(dirname $(realpath $0))

# program installation
sudo pacman -S \
	acpi \
	acpilight \
	alacritty \
	alsa-utils \
	bash-language-server \
	bc \
	biber \
	calibre \
	ctags \
	cups \
	curl \
	dmenu \
	dunst \
	fd \
	ffmpeg \
	firefox-developer-edition \
	flameshot \
	fzf \
	geckodriver \
	git \
	git-lfs \
	htop \
	ipython \
	i3 \
	i3blocks \
	imv \
	jupyterlab-code-formatter \
	jupyterlab-midnightsea-theme \
	jupyterlab-vim \
	libnotify \
	libreoffice-fresh \
	lua-language-server \
	mpv \
	neovim \
	networkmanager \
	net-tools \
	noto-fonts-emoji \
	npm \
	obsidian \
	openssh \
	otf-latinmodern-math \
	pandoc \
	pass \
	physlock \
	podman \
	pulseaudio \
	pulseaudio-alsa \
	pulseaudio-bluetooth \
	pulsemixer \
	python-neovim \
	python-pip \
	python-virtualenv \
	python-virtualenvwrapper \
	ripgrep \
	rsync \
	ruff-lsp \
	shellcheck \
	shfmt \
	systemd-resolvconf \
	sysstat \
	texlab \
	texlive-langfrench \
	texlive-meta \
	tmux \
	ttf-liberation \
	ttf-linux-libertine \
	unclutter \
	unzip \
	uv \
	wget \
	words \
	xcape \
	xclip \
	xcompmgr \
	xorg \
	xwallpaper \
	wireguard-tools \
	yaml-language-server \
	yarn \
	zathura \
	zathura-pdf-mupdf \
	zsh-completions

# aur program installation
if pacman -Qs yay >/dev/null; then
	:
else
	git clone https://aur.archlinux.org/yay.git
	cd yay
	makepkg -si
	cd ..
	rm -rf yay
fi
for prog in \
	boxes \
	brother-hll2370dw \
	efm-langserver \
	languageclient-neovim \
	ltex-ls-bin \
	otf-openmoji \
	spotify \
	write-good \
	zsh-theme-powerlevel10k; do
	if pacman -Qs $prog >/dev/null; then
		:
	else
		yay -S $prog
	fi
done

sudo ln -sf /bin/firefox-developer-edition /bin/firefox

if [ ! -d ~/.config/alacritty ]; then
	mkdir ~/.config/alacritty
fi
ln -sf ~/documents/computer/dotfiles/alacritty/alacritty.toml ~/.config/alacritty/alacritty.toml

# python packages
pip install --user -r $DIR/python/requirements.txt

# npm packages
for package in \
	diagognstic-languageserver \
	eslint \
	prettier \
	stylelint \
	stylelint-order \
	tree-sitter-cli \
	typescript \
	typescript-language-server \
	vscode-langservers-extracted; do
	yarn global add $package
done

# personal scripts
if [ ! -d ~/documents/code/scripts ]; then
	git clone https://gitlab.com/antoinelb/scripts ~/documents/code/scripts
fi

# config dir
if [ ! -d ~/.config ]; then
	mkdir ~/.config
fi

# zsh
if [ ! -d ~/.config/zsh ]; then
	mkdir ~/.config/zsh
fi
ln -sf $DIR"/zsh/zshrc" ~/.zshrc
ln -sf $DIR"/zsh/zprofile" ~/.zprofile
ln -sf $DIR"/zsh/aliases" ~/.config/zsh/aliases
ln -sf $DIR"/zsh/functions" ~/.config/zsh/functions
ln -sf $DIR"/zsh/env" ~/.config/zsh/env
ln -sf $DIR"/zsh/plugins" ~/.config/zsh/plugins
ln -sf $DIR"/zsh/prompt" ~/.config/zsh/prompt
if pacman -Qs zplug >/dev/null; then
	:
else
	curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
fi

# neovim
if [ ! -d ~/.config/nvim ]; then
	mkdir ~/.config/nvim
fi
ln -sf "$DIR/nvim/init.lua" ~/.config/nvim
ln -sf "$DIR/nvim/templates" ~/.config/nvim/
#ln -sf "$DIR/nvim-lua/ftplugin" ~/.config/nvim/

# tmux
ln -sf $DIR"/tmux/tmux.conf" ~/.tmux.conf
if [ ! -d ~/.tmux ]; then
	mkdir ~/.tmux
fi
if [ ! -d ~/.tmux/plugins ]; then
	mkdir ~/.tmux/plugins
fi
ln -sf $DIR"/tmux/tpm" ~/.tmux/plugins/tpm

# fonts
if [ ! -d ~/.local/share/fonts ]; then
	mkdir ~/.local/share/fonts
fi
ln -sf $DIR"/fonts/dank_mono" ~/.local/share/fonts/
ln -sf $DIR"/fonts/cormorant_garamond" ~/.local/share/fonts/
fc-cache

# xorg
if [ ! -f /etc/X11/xorg.conf.d/00-keyboard.conf ]; then
	sudo cp $DIR"/xorg/00-keyboard.conf" /etc/X11/xorg.conf.d/00-keyboard.conf
fi
if [ ! -f /etc/X11/xorg.conf.d/10-monitor.conf ]; then
	sudo cp $DIR"/xorg/10-monitor.conf" /etc/X11/xorg.conf.d/10-monitor.conf
fi
if [ ! -f /etc/X11/xorg.conf.d/30-touchpad.conf ]; then
	sudo cp $DIR"/xorg/30-touchpad.conf" /etc/X11/xorg.conf.d/30-touchpad.conf
fi
if [ ! -f /etc/X11/xorg.conf.d/20-intel.conf ]; then
	sudo cp $DIR"/xorg/20-intel.conf" /etc/X11/xorg.conf.d/20-intel.conf
fi
ln -sf $DIR"/xorg/xinitrc" ~/.xinitrc

# zathura
if [ ! -d ~/.config/zathura ]; then
	mkdir ~/.config/zathura
fi
ln -sf $DIR"/zathura/zathurarc" ~/.config/zathura/zathurarc

# i3
if [ ! -d ~/.config/i3 ]; then
	mkdir ~/.config/i3
fi
ln -sf $DIR"/i3/config" ~/.config/i3/config
ln -sf $DIR"/i3/i3blocks.conf" ~/.config/i3/i3blocks.conf
ln -sf $DIR"/i3/i3blocks" ~/.config/i3/i3blocks
ln -sf $DIR"/i3/lock.sh" ~/.config/i3/lock.sh

# wallpaper
ln -sf $DIR"/wallpapers/aurora.png" ~/.config/wallpaper.png

# default applications
ln -sf $DIR"/mime/mimeapps.list" ~/.config/mimeapps.list
ln -sf $DIR"/mime/text-markdown.xml" ~/.local/share/mime/packages/text-markdown.xml
update-mime-database ~/.local/share/mime

# video player
ln -sf $DIR"/mpv" ~/.config/mpv

# jupyter
if [ ! -d ~/.ipython/profile_default ]; then
	mkdir -p ~/.ipython/profile_default
fi
if [ ! -d ~/.jupyter ]; then
	mkdir -p ~/.jupyter
fi
ln -sf $DIR"/jupyter/ipython_config.py" ~/.ipython/profile_default/ipython_config.py
ln -sf $DIR"/jupyter/jupyter_notebook_config.py" ~/.jupyter/jupyter_notebook_config.py
ln -sf $DIR"/jupyter/jupyter_server_config.py" ~/.jupyter/jupyter_server_config.py

# notifications
if [ ! -d ~/.config/dunst ]; then
	mkdir ~/.config/dunst
fi
ln -sf $DIR"/dunst/dunstrc" ~/.config/dunst/dunstrc

# podman
echo antoine:100000:65536 | sudo tee /etc/subuid >/dev/null
echo antoine:100000:65536 | sudo tee /etc/subgid >/dev/null

# imv
if [ ! -d ~/.config/imv ]; then
	mkdir -p ~/.config/imv
fi
ln -sf $DIR"/imv/config" ~/.config/imv/config

# web
ln -sf $DIR"/stylelint/stylelintrc" ~/.stylelintrc

# reinstall i3 (to prevent bugs)
sudo pacman -S i3
