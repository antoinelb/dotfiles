let b:surround_{char2nr("v")} = "{{ \r }}"
let b:surround_{char2nr("{")} = "{{ \r }}"
let b:surround_{char2nr("%")} = "{% \r %}"
let b:surround_{char2nr("b")} = "{% block \1block name: \1 %}\r{% endblock \1\1 %}"
let b:surround_{char2nr("i")} = "{% if \1condition: \1 %}\r{% endif %}"
let b:surround_{char2nr("w")} = "{% with \1with: \1 %}\r{% endwith %}"
let b:surround_{char2nr("f")} = "{% for \1for loop: \1 %}\r{% endfor %}"
let b:surround_{char2nr("c")} = "{% comment %}\r{% endcomment %}"

if exists("b:did_ftplugin") || (!exists ("g:xmledit_enable_html")) || g:xmledit_enable_html != 1
    finish
endif

let b:html_mode = 1

if !exists("*HtmlAttribCallback")
    function HtmlAttribCallback( xml_tag )
      return 0
    endfunction
endif

runtime ftplugin/xml.vim
