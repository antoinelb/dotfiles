autocmd BufNewFile,BufReadPost *.md set filetype=markdown
let g:vim_markdown_conceal = 0
let g:vim_markdown_math = 0
let g:vim_markdown_new_list_item_indent = 0

au BufNewFile,BufRead *.md,*.txt:
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set expandtab |
    \ set smarttab |
    \ set fileformat=unix |
    \ set conceallevel=2 |
    \ set textwidth=79
