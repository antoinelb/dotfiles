au BufNewFile,BufRead *.R,*.r:
    \ set foldmethod=manual |
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set smarttab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set conceallevel=1

let R_assign = 0
let R_min_editor_width = 80
let R_rconsole_width = 1000
let R_show_args = 0
