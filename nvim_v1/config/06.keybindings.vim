let mapleader = ","

" scroll when moving vertically
nnoremap j gjzz
nnoremap k gkzz

" adapt to canadian multilingual keyboard
nnoremap é /
nnoremap qé q/

" move through linter errors
nmap <silent> <C-h> <Plug>(ale_previous_wrap)
nmap <silent> <C-l> <Plug>(ale_next_wrap)

" insert and delete empty lines
nnoremap <silent><C-j> ma:silent +g/\m^\s*$/d<cr>`ama:noh<cr>
nnoremap <silent><C-k> ma:silent -g/\m^\s*$/d<cr>`ama:noh<cr>
nnoremap <silent><A-j> :set paste <cr>o<esc>k:set nopaste<cr>
nnoremap <silent><A-k> :set paste <cr>O<esc>j:set nopaste<cr>

" change autocomplete enter behavior
inoremap <expr> <cr> (pumvisible() ? "\<c-y>\<cr>" : "\<cr>")

" change movement to respect snake case and camel case
map w <Plug>CamelCaseMotion_w
map b <Plug>CamelCaseMotion_b
map e <Plug>CamelCaseMotion_e
sunmap w
sunmap b
sunmap e

" add comment box around visual selection
vmap <leader>b !boxes<cr>

" language client
nnoremap <f4> :call LanguageClient_contextMenu()<cr>
nnoremap <silent> K :call LanguageClient_textDocument_hover()<cr>
nnoremap <silent> gd :call LanguageClient_textDocument_definition()<cr>
nnoremap <silent> gr :call LanguageClient_textDocument_references()<cr>
nnoremap <silent> gs :call LanguageClient_textDocument_documentSymbol()<cr>
nnoremap <silent> <f2> :call LanguageClient_textDocument_rename()<cr>
nnoremap <silent> gf :call LanguageClient_textDocument_codeAction()<cr>
nnoremap <silent> gc :call LanguageClient_textDocument_formatting()<cr>

" tmux integration
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <c-a>h :TmuxNavigateLeft<cr>
nnoremap <silent> <c-a>j :TmuxNavigateDown<cr>
nnoremap <silent> <c-a>k :TmuxNavigateUp<cr>
nnoremap <silent> <c-a>l :TmuxNavigateRight<cr>
nnoremap <silent><c-s> <c-a>

" folding
nnoremap <space> za
nnoremap <c-space> zA
vnoremap <space> zf

" snippets
imap <expr><cr> neosnippet#expandable()
      \ ? "\<plug>(neosnippet_expand_or_jump)"
      \ : "\<cr>"
smap <expr><cr> neosnippet#expandable()
      \ ? "\<plug>(neosnippet_expand_or_jump)"
      \ : "\<cr>"
xmap <expr><cr> neosnippet#expandable()
      \ ? "\<plug>(neosnippet_expand)"
      \ : "\<cr>"
imap <expr><tab> pumvisible()
      \ ? "\<c-n>"
      \ : neosnippet#expandable_or_jumpable()
      \ ? "\<plug>(neosnippet_expand_or_jump)"
      \ : "\<tab>"
smap <expr><tab> neosnippet#expandable_or_jumpable()
      \ ? "\<plug>(neosnippet_expand_or_jump)"
      \ : "\<tab>"

" conceal markers
if has("conceal")
  set conceallevel=2 concealcursor=niv
endif

" align
xmap ga <plug>(EasyAlign)
nmap ga <plug>(EasyAlign)

" autopairs
let g:AutoPairsShortcutToggle = "<leader>p"

" testing
nmap <silent> <leader>tn :TestNearest<cr>
nmap <silent> <leader>tf :TestFile<cr>
nmap <silent> <leader>tl :TestLast<cr>

" shortcuts
autocmd BufWritePost ~/documents/computer/config/shortcuts/shortcuts !sh ~/documents/code/scripts/sync-shortcuts.sh

" denite
nmap ; :Denite buffer -split=float -winrow=1<cr>
nnoremap <c-f> :Denite file/rec -split=floating -winrow=1<cr>
nnoremap <c-p> :<c-u>Denite grep:. -no-empty -mode=normal<cr>
nnoremap <c-w> :<c-u>DeniteCursorWord grep:. -mode=normal<cr>
