au BufNewFile,BufRead *.js,*.html,*.css,*.json,*.jsx,*.scss:
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set smarttab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set conceallevel=1

au BufNewFile,BufRead *.js,*.css,*.json,*.jsx,*.scss:
    \ set foldmethod=syntax

au BufNewFile,BufRead *.html:
    \ set foldmethod=indent

" format code
let g:neoformat_css_stylelint = {
        \ 'exe': 'stylelint',
        \ 'args': ['--fix'],
        \ 'stdin': 1
        \ }
let g:neoformat_scss_stylelint = {
        \ 'exe': 'stylelint',
        \ 'args': ['--fix'],
        \ 'stdin': 1
        \ }
let g:neoformat_enabled_css = ["prettier", "stylelint"]
let g:neoformat_enabled_scss = ["prettier", "stylelint"]
let g:neoformat_enabled_javascript = ["prettier", "prettier-eslint"]
let g:neoformat_enabled_json = ["prettier"]
let g:neoformat_enabled_markdown = ["prettier"]
let g:neoformat_enabled_html = ["prettier"]
autocmd filetype css nnoremap <localleader>= :Neoformat! css<cr>
autocmd filetype scss nnoremap <localleader>= :Neoformat! scss<cr>
autocmd filetype javascript nnoremap <localleader>= :Neoformat! javascript<cr>
autocmd filetype json nnoremap <localleader>= :Neoformat! json<cr>
autocmd filetype markdown nnoremap <localleader>= :Neoformat! markdown<cr>
autocmd filetype html nnoremap <localleader>= :Neoformat! html<cr>
autocmd bufwritepre *.css silent execute ":Neoformat! css"
autocmd bufwritepre *.scss silent execute ":Neoformat! scss"
autocmd bufwritepre *.js,*.jsx silent execute ":Neoformat! javascript"
autocmd bufwritepre *.json silent execute ":Neoformat! json"

let g:javascript_plugin_flow = 1

" allow jsx in js files
let g:jsx_ext_required = 0

" html5 syntax options
let g:html5_event_handler_attributes_complete = 0
let g:html5_rdfa_attributes_complete = 0
let g:html5_microdata_attributes_complete = 0
let g:html5_aria_attributes_complete = 0

let g:xmledit_enable_html = 1

function HtmlAttribCallback(xml_tag)
  return 0
endfunction

let g:deoplete#sources#ternjs#types = 1

let g:user_emmet_install_global = 0
autocmd FileType html,css,htmldjango,scss EmmetInstall
let g:user_emmet_leader_key="<c-c>"
let g:user_emmet_settings = {
      \ "html": {
        \ "block_all_childless": 1
      \}
\}
