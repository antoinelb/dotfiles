let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_smart_case = 1
let g:deoplete#max_list = 5
let g:deoplete#auto_complete_delay = 5
let g:echodoc#enable_at_startup = 1
let g:echodoc#enable_force_overwrite = 1

" disable autocomplete by default
let g:deoplete_disable_auto_complete = 1
let b:deoplete_disable_auto_complete = 1

call deoplete#custom#source("LanguageClient", "mark", " ")
call deoplete#custom#source("omni",           "mark", "⌾")
call deoplete#custom#source("jedi",           "mark", "⌁")
call deoplete#custom#source("vim",            "mark", "⌁")
call deoplete#custom#source("syntax",         "mark", "♯")
call deoplete#custom#source("member",         "mark", ".")

call deoplete#custom#source("LanguageClient",
      \ "min_pattern_length",
      \ 2,
      \ "sorters",
      \ [])
"
if !exists("g:deoplete#omni#input_patterns")
  let g:deoplete#omni#input_patterns = {}
endif

" disable candidates in Comment/String syntax
call deoplete#custom#source("-",
      \ "disabled_syntaxes", ["Comment", "String"])

autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

call deoplete#custom#option("sources", {
      \ "python": ["jedi"],
      \ "vim": ["vim"],
      \ })

" ignores sources
" let g:deoplete#ignore_sources = {}
" let g:deoplete#ignore_sources._ = ["buffer", "around", "tag"]

" options
set completeopt-=preview
set completeopt+=noinsert
set completeopt+=noselect
set shortmess+=c

" tab to switch options
inoremap <silent><expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
inoremap <silent><expr><s-tab> pumvisible() ? "\<C-p>" : "\<tab>"

" function! s:check_back_space() abort
  " let col = col('.') - 1
  " return !col || getline('.')[col - 1]  =~ '\s'
" endfunction
"
" inoremap <silent><expr> <TAB>
      " \ pumvisible() ? "\<C-n>" :
      " \ <SID>check_back_space() ? "\<TAB>" :
      " \ coc#refresh()
"
" autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif
"
" set hidden
" set nobackup
" set nowritebackup
" set cmdheight=2
" set updatetime=300
" set shortmess+=c
" set signcolumn=yes
"
" inoremap <silent><expr> <TAB>
      " \ pumvisible() ? "\<C-n>" :
      " \ <SID>check_back_space() ? "\<TAB>" :
      " \ coc#refresh()
" inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
"
" function! s:check_back_space() abort
  " let col = col('.') - 1
  " return !col || getline('.')[col - 1]  =~# '\s'
" endfunction
"
" inoremap <silent><expr> <c-space> coc#refresh()
"
" inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
"
" nmap <silent> [c <Plug>(coc-diagnostic-prev)
" nmap <silent> ]c <Plug>(coc-diagnostic-next)
"
" nmap <silent> gd <Plug>(coc-definition)
" nmap <silent> gy <Plug>(coc-type-definition)
" nmap <silent> gi <Plug>(coc-implementation)
" nmap <silent> gr <Plug>(coc-references)
"
" nnoremap <silent> K :call <SID>show_documentation()<CR>
"
" function! s:show_documentation()
  " if (index(['vim','help'], &filetype) >= 0)
    " execute 'h '.expand('<cword>')
  " else
    " call CocAction('doHover')
  " endif
" endfunction
"
"
" nmap <leader>rn <Plug>(coc-rename)
"
" vmap <leader>f  <Plug>(coc-format-selected)
" nmap <leader>f  <Plug>(coc-format-selected)
"
" augroup mygroup
  " autocmd!
  " " Setup formatexpr specified filetype(s).
  " autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " " Update signature help on jump placeholder
  " autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
" augroup end
"
" vmap <leader>a  <Plug>(coc-codeaction-selected)
" nmap <leader>a  <Plug>(coc-codeaction-selected)
"
" nmap <leader>ac  <Plug>(coc-codeaction)
" nmap <leader>qf  <Plug>(coc-fix-current)
"
" command! -nargs=0 Format :call CocAction('format')
"
" command! -nargs=? Fold :call     CocAction('fold', <f-args>)
"
" nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
