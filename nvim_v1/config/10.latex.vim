let maplocalleader=","
let g:tex_flavor='latex'
let g:vimtex_compiler_method='latexmk'
let g:vimtex_fold_enabled = 1
let g:vimtex_view_method = "zathura"

au BufNewFile,BufRead *.tex,*.bib:
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set smarttab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set textwidth=79 

autocmd FileType tex nnoremap <LocalLeader>s :set spell spelllang=fr<cr>

" format code
let g:neoformat_enabled_latex = ["latexindent"]
autocmd filetype tex nnoremap <localleader>= :Neoformat! latex<cr>
autocmd bufwritepre, *.tex silent execute ":Neoformat! latex"

let g:tex_conceal = 0

nnoremap ,tex :-1read $HOME/.config/nvim/templates/skeleton.tex<cr>zR11Gf{a
