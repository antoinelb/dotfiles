let g:python3_host_prog='/usr/bin/python3'
let g:python2_host_prog='/usr/bin/python2'
let g:python_host_prog='/usr/bin/python'

au BufNewFile,BufRead *.py:
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set expandtab |
    \ set smarttab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set foldlevel=1 |
    \ set textwidth=0

let python_highlight_all=1

augroup PythonCustomization
  " highlight python self, when followed by a comma, a period or a parenth
   :autocmd FileType python syn match pythonStatement "\(\W\|^\)\@<=self\([\.,)]\)\@="
augroup END

let g:jedi#completions_enabled = 0
let g:jedi#show_call_signatures = "2"

" format code
let g:neoformat_python_black = {
      \ "exe": "black",
      \ "args": ["--line-length 79", "-"],
      \ "stdin": 1
      \}
let g:neoformat_python_pyment = {
      \ "exe": "pyment",
      \ "args": ["--output numpydoc", "--convert", "--write", "--first-line False", "-"],
      \ "stdin": 1
      \}
let g:neoformat_enabled_python = ["isort", "black"]
let g:neoformat_run_all_formatters = 1
autocmd filetype python nnoremap <localleader>= :Neoformat! python<cr>
autocmd filetype python vnoremap <localleader>b :!boxes -d shell<cr>
autocmd bufwritepre, *.py silent execute ":Neoformat! python"

" sort imports
autocmd FileType python nnoremap <LocalLeader>i :!isort %<CR><CR>

" django
autocmd filetype html nnoremap <silent> <localleader>dj :set ft=htmldjango<cr>
