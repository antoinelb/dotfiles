call denite#custom#var("file/rec", "command", ["rg", "--files", "--glob", "!.git"])
call denite#custom#var("grep", "command", ["rg"])
call denite#custom#var("grep", "default_opts", ["--hidden", "--vimgrep", "--heading", "-S"])
call denite#custom#var("grep", "recursive_opts", [])
call denite#custom#var("grep", "pattern_opt", ["--regexp"])
call denite#custom#var("grep", "separator", ["--"])
call denite#custom#var("grep", "final_opts", [])
call denite#custom#var("buffer", "date_format", "")
let s:denite_options = {"default": {
      \ "auto_resize": 1,
      \ "direction": "rightbelow",
      \ "highlight_matched_char": "Function",
      \ "highlight_matched_range": "Normal",
      \ "highlight_mode_insert": "Visual",
      \ "highlight_mode_normal": "Visual",
      \ "prompt": "λ:",
      \ "prompt_highlight": "Function",
      \ "root-markers": [".git"],
      \ "winminheight": "10"
\ }}
function! s:profile(opts) abort
  for l:fname in keys(a:opts)
    for l:dopt in keys(a:opts[l:fname])
      call denite#custom#option(l:fname, l:dopt, a:opts[l:fname][l:dopt])
    endfor
  endfor
endfunction

try
  call s:profile(s:denite_options)
catch
  echo 'Denite not installed. It should work after running :PlugInstall'
endtry
