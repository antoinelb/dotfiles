let g:sqlutil_keyword_case = '\U'
let g:sqlutil_align_comma = 1
let g:sqlutil_align_first_word = 1
let g:sqlutil_align_keyword_right = 1
let g:sqlutil_align_where = 1
autocmd bufwritepre,filewritepre *.sql silent execute ":%SQLUFormatter<cr>"
